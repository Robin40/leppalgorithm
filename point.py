from math import copysign, hypot


class Point(object):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return "•({}, {})".format(self.x, self.y)

    def __eq__(self, other: 'Point') -> bool:
        return (self.x, self.y) == (other.x, other.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __add__(self, other: 'Point') -> 'Point':
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'Point') -> 'Point':
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, scale: float) -> 'Point':
        return Point(self.x * scale, self.y * scale)

    def __truediv__(self, scale: float) -> 'Point':
        return Point(self.x / scale, self.y / scale)

    def __lt__(self, other: 'Point') -> bool:
        return (self.x, self.y) < (other.x, other.y)

    def rot90(self) -> 'Point':
        return Point(-self.y, self.x)

    def rot270(self) -> 'Point':
        return Point(self.y, -self.x)

    def cross(self, other: 'Point') -> float:
        return self.x * other.y - self.y * other.x

    def norm(self) -> float:
        return hypot(self.x, self.y)

    def distance_to(self, other: 'Point') -> float:
        return (other - self).norm()

    def sq_norm(self) -> float:
        return self.x * self.x + self.y * self.y

    def sq_distance_to(self, other: 'Point') -> float:
        return (other - self).sq_norm()


def pos(p: Point) -> tuple:
    return int(p.x), int(p.y)
