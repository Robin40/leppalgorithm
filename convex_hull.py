from typing import Iterable, List

from float import eps
from point import Point
from polygon import Polygon
from segment import Segment


def convex_hull(points: Iterable[Point]) -> Polygon:
    points = sorted(points)
    assert len(points) >= 3

    # noinspection PyShadowingNames
    def find_lower_hull(points: Iterable[Point]) -> List[Point]:
        hull = []
        for p in points:
            while len(hull) >= 2 and Segment(hull[-2], hull[-1]).cross(p) < -eps:
                hull.pop()
            hull.append(p)
        return hull

    lower_hull = find_lower_hull(points)
    upper_hull = find_lower_hull(reversed(points))
    return Polygon(lower_hull[:-1] + upper_hull[:-1])
