from math import inf
from typing import Dict, List, Set

from pyrsistent import pmap, pset

from canvas import Canvas
from mesh import Mesh
from point import Point
from polygon import Polygon
from segment import Segment

"""
def clockwiseness(uv: Segment, w: Point) -> Tuple[int, float]:
    vu = uv.inv()
    quadrant_begin = vu
    quadrant_end = vu.rot270()

    for quadrant_number in range(4):
        begin_cross = quadrant_begin.cross(w)
        end_cross = quadrant_end.cross(w)
        # noinspection PyChainedComparisons
        if begin_cross < -eps and end_cross >= -eps:
            return quadrant_number, -begin_cross

        quadrant_begin = quadrant_begin.rot270()
        quadrant_end = quadrant_end.rot270()

    assert False
"""


def clockwiseness(uv: Segment, w: Point):
    u, v = uv.a, uv.b
    if u == w:
        return inf
    return -Segment(v, u).ccw_angle_of(w)


def canvas_to_mesh(canvas: Canvas) -> Mesh:
    ccw: Dict[Segment, Segment] = {}
    for uv in canvas.iter_half_edges():
        v = uv.b
        candidates = canvas.neighbors_of[v]
        less_clockwise = min(candidates, key=lambda w: clockwiseness(uv, w))
        ccw[uv] = Segment(v, less_clockwise)

    polygons: Set[Polygon] = set()
    visited: Set[Segment] = set()
    points: List[Point] = []

    def dfs(node: Segment) -> None:
        if node in visited:
            return
        visited.add(node)
        points.append(node.a)
        dfs(ccw[node])

    for s in canvas.iter_half_edges():
        points = []
        if s not in visited:
            dfs(s)
            polygons.add(Polygon(points))

    return Mesh(pmap(ccw), pset(polygons))
