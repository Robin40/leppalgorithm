from canvas import Canvas
from triangulation import Triangulation


def triangulation_to_canvas(mesh: Triangulation) -> Canvas:
    canvas = Canvas()
    for half_edge in mesh.ccw:
        u, v = half_edge.a, half_edge.b
        canvas.points.add(u)
        canvas.points.add(v)
        canvas.neighbors_of[u].add(v)
        canvas.neighbors_of[v].add(u)
    return canvas
