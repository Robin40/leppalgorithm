Requirements: python 3, pygame, pyrsistent, numpy
Controls: Left click to add point, click two points to add/remove segment, right click to remove point, ENTER to refine triangulation.

How to use the program:
Execute main.py using python 3 and use the interface that I made to add points and segments (you have to create the mesh instead of loading it from a file).

How to use the canvas interface:
Left click creates a point,
Right click deletes a point,
Clicking a point and then another creates a segment (or deletes it if it already exists).

This isn't the fastest interface in the world but it's the one I implemented.

If the mesh is a valid triangulation, it'll be fully painted cyan. The (debug) yellow lines correspond to the convex hull of the point set. If two segments intersect, the mesh is no longer valid and a cross will appear signaling the intersection.

Yes, I know that loading triangles from a file could've been easier, but a complex interface is way more fun to debug.

When your (triangulation) mesh is ready, press ENTER to refine the triangulation. If the mesh is not a valid triangulation the program will print "not a triangulation" in console. If it's valid, the refined version will be showed in screen, and the steps followed by the algorithm will be reported on console.

The program works well (is I know), except for floating-point precision issues. There are some workarounds like using epsilons, and stopping the algorithm when it creates (too small) degenerated triangles.
