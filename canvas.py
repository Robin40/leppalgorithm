from collections import defaultdict
from typing import DefaultDict, Generator, Optional, Set

import pygame
from pyrsistent import pset

from convex_hull import convex_hull
from float import eps
from point import Point, pos
from segment import Segment

pt_color = pygame.Color('red')
pt_radius = 4

seg_color = pygame.Color('black')
seg_thick = 1

hull_color = pygame.Color('#ffff66')
hull_thick = 5

err_color = pygame.Color('red')
high_color = pygame.Color('cyan')

cross_size = Point(16, 16)
cross_thick = 4


class Canvas(object):
    def __init__(self):
        self.points = set()
        self.neighbors_of: DefaultDict[Set[Point]] = defaultdict(set)
        self.active_p: Optional[Point] = None
        self.highlighted_polygons: pset = pset()

    def select(self, mouse_p: Point) -> Optional[Point]:
        for p in self.points:
            if p.distance_to(mouse_p) <= pt_radius + eps:
                return p

    def add_point(self, p):
        self.points.add(p)
        self.highlighted_polygons = pset()

    def remove_point(self, p):
        u = p
        for v in self.neighbors_of[u]:
            self.neighbors_of[v].remove(u)
        del self.neighbors_of[u]
        self.points.remove(p)

        if self.active_p is p:
            self.active_p = None
        self.highlighted_polygons = pset()

    def left_click(self, mouse_p: Point) -> None:
        p = self.select(mouse_p)
        if p is None:
            self.add_point(mouse_p)
            return

        if self.active_p is None:
            self.active_p = p
            return

        if p == self.active_p:
            self.active_p = None
            return

        u, v = self.active_p, p
        if v not in self.neighbors_of[u]:
            self.neighbors_of[u].add(v)
            self.neighbors_of[v].add(u)
        else:
            self.neighbors_of[u].remove(v)
            self.neighbors_of[v].remove(u)

        self.active_p = None

    def right_click(self, mouse_p: Point) -> None:
        p = self.select(mouse_p)
        if p is None:
            self.active_p = None
            return

        self.remove_point(p)

    def iter_half_edges(self) -> Generator[Segment, None, None]:
        for u in self.points:
            for v in self.neighbors_of[u]:
                yield Segment(u, v)

    def iter_segments(self) -> Generator[Segment, None, None]:
        for s in self.iter_half_edges():
            if s.a < s.b:
                yield s

    def draw(self, surface: pygame.Surface) -> None:
        if len(self.points) >= 3:
            for s in convex_hull(self.points).iter_segments():
                pygame.draw.line(surface, hull_color, pos(s.a), pos(s.b), hull_thick)

        for polygon in self.highlighted_polygons:
            pointlist = [pos(p) for p in polygon.points]
            pygame.draw.polygon(surface, high_color, pointlist)

        for s in self.iter_segments():
            pygame.draw.line(surface, seg_color, pos(s.a), pos(s.b), seg_thick)

        for p in self.points:
            pygame.draw.circle(surface, pt_color, pos(p), pt_radius)

        if self.active_p is not None:
            pygame.draw.circle(surface, pt_color, pos(self.active_p), pt_radius + 3, 1)

        for s in self.iter_segments():
            for t in self.iter_segments():
                segments_points = {s.a, s.b, t.a, t.b}
                if len(segments_points) == 4:
                    p = s.inter(t)
                    if p is not None:
                        pygame.draw.line(surface, err_color, pos(p - cross_size), pos(p + cross_size), cross_thick)
                        pygame.draw.line(surface, err_color, pos(p - cross_size.rot90()), pos(p + cross_size.rot90()),
                                         cross_thick)
