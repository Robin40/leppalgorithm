import sys

import pygame

from canvas import Canvas
from lepp_controller import LeppController
import mouse
from point import Point

assert sys.version_info.major == 3

size = width, height = 1024, 768

pygame.init()
surface = pygame.display.set_mode(size)
bg_color = pygame.Color('white')

canvas = Canvas()

while True:
    for event in pygame.event.get():
        mouse_pos = pygame.mouse.get_pos()
        mouse_p = Point(*mouse_pos)

        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == mouse.LEFT_CLICK:
                canvas.left_click(mouse_p)
            elif event.button == mouse.RIGHT_CLICK:
                canvas.right_click(mouse_p)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                canvas = LeppController(canvas).refine_triangulation()
        elif event.type == pygame.QUIT:
            pygame.quit()
            exit(0)

    LeppController(canvas).check_triangulation()

    surface.fill(bg_color)
    canvas.draw(surface)
    pygame.display.flip()
