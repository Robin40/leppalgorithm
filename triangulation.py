from pyrsistent import pmap, pset

from iteration import iter_rotations_three
from segment import Segment
from triangle import Triangle


class Triangulation(object):
    def __init__(self, triangles: pset = pset(), triangle: pmap = pmap(), ccw: pmap = pmap(), cw: pmap = pmap()):
        self.triangles = triangles
        self.triangle = triangle
        self.ccw = ccw
        self.cw = cw

    def __contains__(self, s: Segment) -> bool:
        return s in self.triangle

    def add_triangle(self, t: Triangle) -> 'Triangulation':
        mesh = self
        for u, ccw, cw in iter_rotations_three(*t.sides):
            mesh = Triangulation(mesh.triangles.add(t),
                                 mesh.triangle.set(u, t),
                                 mesh.ccw.set(u, ccw),
                                 mesh.cw.set(u, cw))
        return mesh

    def remove_triangle(self, t: Triangle) -> 'Triangulation':
        triangle_dict = self.triangle
        ccw_dict = self.ccw
        cw_dict = self.cw
        for side in t.sides:
            triangle_dict = triangle_dict.remove(side)
            ccw_dict = ccw_dict.remove(side)
            cw_dict = cw_dict.remove(side)

        return Triangulation(self.triangles.remove(t),
                             triangle_dict,
                             ccw_dict,
                             cw_dict)
