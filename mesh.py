from pyrsistent import pmap, pset


class Mesh(object):
    def __init__(self, ccw: pmap = pmap(), polygons: pset = pset()):
        self.ccw = ccw
        self.polygons = polygons
