from typing import Generator, Tuple

from float import eps
from point import Point
from segment import Segment


class Triangle(object):
    def __init__(self, *points):
        a, b, c = points
        assert Triangle.is_ccw(a, b, c)

        self.points = points
        self.sides = [Segment(a, b), Segment(b, c), Segment(c, a)]

    @staticmethod
    def is_ccw(a: Point, b: Point, c: Point) -> bool:
        return Segment(a, b).cross(c) > eps

    def area(self) -> float:
        a, b, c = self.points
        return Segment(a, b).cross(c) * .5

    def centroid(self) -> Point:
        xs = (p.x for p in self.points)
        ys = (p.y for p in self.points)
        return Point(sum(xs) / 3, sum(ys) / 3)
