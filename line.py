from typing import Optional

from float import eps
from point import Point


class Line(object):
    def __init__(self, s: 'Segment'):
        self.a = s.b.y - s.a.y
        self.b = s.a.x - s.b.x
        self.c = -self.a * s.a.x - self.b * s.a.y

    def inter(self, other: 'Line') -> Optional[Point]:
        div = self.a * other.b - self.b * other.a
        if abs(div) <= eps:
            return None

        return Point((self.b * other.c - other.b * self.c) / div,
                     (other.a * self.c - self.a * other.c) / div)
