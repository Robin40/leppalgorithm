from functools import reduce

from mesh import Mesh
from polygon import Polygon
from triangle import Triangle
from triangulation import Triangulation


def is_ccw_triangle(poly: Polygon) -> bool:
    if len(poly.points) != 3:
        return False
    return Triangle.is_ccw(*poly.points)


def mesh_to_triangulation(mesh: Mesh) -> Triangulation:
    ccw_triangles = (Triangle(*poly.points) for poly in mesh.polygons if is_ccw_triangle(poly))

    return reduce(lambda triangulation, t: triangulation.add_triangle(t),
                  ccw_triangles, Triangulation())
