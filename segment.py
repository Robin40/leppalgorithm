from math import atan2, pi, tau
from typing import Optional

from float import eps
from line import Line
from point import Point


class Segment(object):
    def __init__(self, a: Point, b: Point):
        self.a = a
        self.b = b

    def __repr__(self) -> str:
        return "Segment({}, {})".format(self.a, self.b)

    def __eq__(self, other: 'Segment') -> bool:
        return (self.a, self.b) == (other.a, other.b)

    def __hash__(self):
        return hash((self.a, self.b))

    def inv(self) -> 'Segment':
        return Segment(self.b, self.a)

    def rot270(self) -> 'Segment':
        return Segment(self.a, self.a + (self.b - self.a).rot270())

    def cross(self, p: Point) -> float:
        return (self.b - self.a).cross(p - self.a)

    def sq_length(self) -> float:
        return self.a.sq_distance_to(self.b)

    def ccw_angle(self) -> float:
        return atan2(self.b.y - self.a.y, self.b.x - self.a.x) + pi

    def ccw_angle_of(self, p: Point) -> float:
        alpha, beta = self.ccw_angle(), Segment(self.a, p).ccw_angle()
        if alpha < beta:
            return beta - alpha
        return beta - alpha + tau

    def intersects(self, other: 'Segment') -> bool:
        return self.cross(other.a)*self.cross(other.b) <= eps and \
            other.cross(self.a)*other.cross(self.b) <= eps

    def inter(self, other: 'Segment') -> Optional[Point]:
        if self.intersects(other):
            return Line(self).inter(Line(other))
        return None
