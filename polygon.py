from typing import Generator, List

from point import Point
from segment import Segment


class Polygon(object):
    def __init__(self, points: List[Point]):
        self.points = points

    def __repr__(self) -> str:
        return "Polygon({})".format(self.points)

    def iter_segments(self) -> Generator[Segment, None, None]:
        n = len(self.points)
        for i in range(n):
            a, b = self.points[i], self.points[(i+1) % n]
            yield Segment(a, b)
