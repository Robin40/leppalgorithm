from typing import Iterable, Tuple, TypeVar

T = TypeVar('T')


def iter_rotations_three(a: T, b: T, c: T) -> Iterable[Tuple[T, T, T]]:
    yield a, b, c
    yield b, c, a
    yield c, a, b
