from math import pi
from typing import Optional

from pyrsistent import pset

from canvas import Canvas
from canvas_to_mesh import canvas_to_mesh
from convex_hull import convex_hull
from lepp_algorithm import refine_triangulation
from mesh import Mesh
from mesh_to_triangulation import is_ccw_triangle, mesh_to_triangulation
from triangulation_to_canvas import triangulation_to_canvas


class LeppController(object):
    def __init__(self, canvas: Canvas):
        self.canvas = canvas

    def check_triangulation(self) -> Optional[Mesh]:
        mesh = canvas_to_mesh(self.canvas)
        ccw_triangles = list(filter(is_ccw_triangle, mesh.polygons))
        self.canvas.highlighted_polygons = pset(ccw_triangles)

        is_only_triangles = len(ccw_triangles) + 1 == len(mesh.polygons)
        points = self.canvas.points
        contains_convex_hull = len(points) >= 3 and \
            all(side in mesh.ccw for side in convex_hull(points).iter_segments())
        is_triangulation = is_only_triangles and contains_convex_hull

        return mesh if is_triangulation else None

    def refine_triangulation(self) -> Canvas:
        mesh = self.check_triangulation()
        if mesh is None:
            print("not a triangulation")
            return self.canvas

        old_triangulation = mesh_to_triangulation(mesh)
        refined = refine_triangulation(tolerance=pi / 6, max_iterations=100, mesh=old_triangulation)
        return triangulation_to_canvas(refined)
