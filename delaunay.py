from typing import Any, Tuple

from numpy.linalg import det

from float import eps
from point import Point
from segment import Segment
from triangle import Triangle
from triangulation import Triangulation


def circle_test(*points) -> Any:
    return det([[p.x, p.y, p.sq_norm(), 1] for p in points])


def are_locally_delaunay(a: Point, b: Point, c: Point, d: Point) -> bool:
    return circle_test(a, b, c, d) <= eps


def find_quad(side: Segment, neigh: Segment, mesh: Triangulation) -> Tuple[Segment, Segment, Segment, Segment]:
    return mesh.ccw[side], mesh.cw[side], mesh.ccw[neigh], mesh.cw[neigh]


def legalize(side: Segment, mesh: Triangulation) -> Triangulation:
    neigh: Segment = side.inv()
    if neigh not in mesh:
        return mesh

    quad_sides = find_quad(side, neigh, mesh)
    a, b, c, d = (side.a for side in quad_sides)
    if are_locally_delaunay(a, b, c, d):
        return mesh

    bcd, dab = Triangle(b, c, d), Triangle(d, a, b)

    mesh = mesh \
        .remove_triangle(mesh.triangle[side]) \
        .remove_triangle(mesh.triangle[neigh]) \
        .add_triangle(bcd) \
        .add_triangle(dab)

    for side in quad_sides:
        mesh = legalize(side, mesh)

    return mesh
