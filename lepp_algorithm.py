from typing import Iterable, List, Optional, Tuple

from builtins import int

from delaunay import find_quad, are_locally_delaunay, legalize
from float import eps
from iteration import iter_rotations_three
from point import Point
from segment import Segment
from triangle import Triangle
from triangulation import Triangulation


def worst_angle(t: Triangle) -> float:
    def angle(points: Tuple[Point, Point, Point]) -> float:
        a, b, c = points
        return Segment(a, b).ccw_angle_of(c)

    angles = map(angle, iter_rotations_three(*t.points))
    return min(angles)


def iter_bad_triangles(tolerance: float, mesh: Triangulation) -> Iterable[Triangle]:
    def is_bad_triangle(t: Triangle) -> bool:
        return worst_angle(t) <= tolerance + eps

    return filter(is_bad_triangle, mesh.triangles)


def longest_side(t: Triangle) -> Segment:
    return max(t.sides, key=Segment.sq_length)


def find_lepp(t0: Triangle, mesh: Triangulation) -> List[Segment]:
    lepp: List[Segment] = []
    prev: Optional[Segment] = None
    t: Optional[Triangle] = t0
    while t is not None:
        side: Segment = longest_side(t)
        lepp.append(side)
        neigh = side.inv()
        if prev is not None and neigh == prev:
            break
        prev = side
        t = mesh.triangle.get(neigh)
    return lepp


def centroid(a: Point, b: Point, c: Point, d: Point) -> Point:
    t1, t2 = Triangle(a, b, c), Triangle(a, c, d)
    c1, c2 = t1.centroid(), t2.centroid()
    a1, a2 = t1.area(), t2.area()
    return (c1 * a1 + c2 * a2) / (a1 + a2)


_iterations = _max_iterations = 0


def log_iteration() -> None:
    global _iterations
    _iterations += 1
    if _iterations >= _max_iterations:
        raise StopIteration


def bisect_border_triangle(terminal_edge: Segment, mesh: Triangulation) -> Triangulation:
    middle: Point = (terminal_edge.a + terminal_edge.b) * .5
    bisection_origin: Point = mesh.ccw[terminal_edge].b
    try:
        mesh = mesh \
            .remove_triangle(mesh.triangle[terminal_edge]) \
            .add_triangle(Triangle(bisection_origin, terminal_edge.a, middle)) \
            .add_triangle(Triangle(bisection_origin, middle, terminal_edge.b))
    except AssertionError:
        pass
    return mesh


def refine_once(t0: Triangle, mesh: Triangulation) -> Triangulation:
    lepp: List[Segment] = find_lepp(t0, mesh)
    assert len(lepp) >= 1

    log_iteration()

    # if len(lepp) == 1:
    #     print("lepp of length 1 - bisection")
    #     return bisect_border_triangle(terminal_edge=lepp[0], mesh=mesh)
    #
    # terminal_edge = lepp[-2]
    # neigh = terminal_edge.inv()
    # assert neigh in mesh

    terminal_edge = lepp[-1]
    neigh = terminal_edge.inv()
    if neigh not in mesh:
        print("terminal edge is border - doing bisection")
        return bisect_border_triangle(terminal_edge, mesh)

    quad_sides = find_quad(terminal_edge, neigh, mesh)
    a, b, c, d = (side.a for side in quad_sides)
    if are_locally_delaunay(a, b, c, d):
        print("locally delaunay - centroid insertion")
        old_mesh = mesh
        try:
            o = centroid(a, b, c, d)
            mesh = mesh \
                .remove_triangle(mesh.triangle[terminal_edge]) \
                .remove_triangle(mesh.triangle[neigh])
            for side in quad_sides:
                mesh = mesh.add_triangle(Triangle(o, side.a, side.b))
            return mesh
        except AssertionError:
            print("centroid insertion failed - doing bisection")
            return bisect_border_triangle(terminal_edge, old_mesh)
    else:
        print("not delaunay - legalize")
        return legalize(terminal_edge, mesh)


def refine_triangle(t0: Triangle, mesh: Triangulation) -> Triangulation:
    while t0 in mesh.triangles:
        mesh = refine_once(t0, mesh)
    return mesh


def refine_triangulation(tolerance: float, max_iterations: int, mesh: Triangulation) -> Triangulation:
    global _iterations, _max_iterations
    _iterations = 0
    _max_iterations = max_iterations
    try:
        needs_refine = True
        while needs_refine:
            needs_refine = False
            for t0 in iter_bad_triangles(tolerance, mesh):
                mesh = refine_triangle(t0, mesh)
                needs_refine = True

    except StopIteration:
        pass
    return mesh
